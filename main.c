#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <portaudio.h>
#include "callback.h"

#ifndef SAMPLE_RATE
#define SAMPLE_RATE 44100
#endif

void errexit(const char *text)
{
	fprintf(stderr, "PortAudio error: %s\n", text);
	exit(1);
}

void fill_wavetable(paTestData *data)
{
	data->int_left_phase = data->int_right_phase = 0;
	for (size_t i = 0; i < TABLE_SIZE; ++i) {
		data->wavetable[i] = (float) sin( ((double)i/(double)TABLE_SIZE) * M_PI * 2. );
	}
}

int main(int argc, char **argv)
{
	PaStream *stream;
	PaError err;
	err = Pa_Initialize();
	if (err != paNoError) 
		errexit(Pa_GetErrorText(err));

	paTestData data;
	fill_wavetable(&data);
#ifndef BYTEBEAT
	err = Pa_OpenDefaultStream(&stream, 0, 2, paFloat32, SAMPLE_RATE, 256, patestCallback, &data);
#else
	err = Pa_OpenDefaultStream(&stream, 0, 2, paInt8, SAMPLE_RATE, 256, patestCallback, &data);
#endif

	err = Pa_StartStream(stream);
	if (err != paNoError)
		errexit(Pa_GetErrorText(err));

	for (;;) {}
	
	err = Pa_Terminate();
	if (err != paNoError)
		errexit(Pa_GetErrorText(err));
}
