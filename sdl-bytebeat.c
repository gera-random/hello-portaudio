#include <SDL2/SDL.h>

int t = 1;

void callback(void *userdata, Uint8 *stream, int len)
{
	for (int i = 0; i < len; ++i) {
		// "the 42 melody"
		stream[i] = t*(42&t>>10);
		++t;
	}
}

int main(int argc, char *argv[])
{
	SDL_Init(SDL_INIT_AUDIO);
	const char *device_name = SDL_GetAudioDeviceName(0, 0);
	SDL_AudioSpec spec_desired = {
		.freq = 8000,
		.format = AUDIO_S8,
		.channels = 1,
		.samples = 1024,
		.callback = callback
	}, spec;
	SDL_AudioDeviceID device = SDL_OpenAudioDevice(device_name, 0, &spec_desired, &spec, 0);
	SDL_PauseAudioDevice(device, 0);
	getc(stdin);
	SDL_CloseAudioDevice(device);
	SDL_Quit();
	return 0;
}
