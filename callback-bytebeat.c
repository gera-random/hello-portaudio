#include <limits.h>
#include "callback.h"
#include <stdio.h>

static char beat(int t)
{
	// "Hard Level" by StephanShi
	return t*(t&16384?7:5)*(5-(3&t>>8))>>(3&-t>>(t&4096?2:16))|t>>(t&16384?t&4096?3:4:3);
}

int patestCallback(const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo *timeInfo, PaStreamCallbackFlags statusFlags, void *userData)
{
	paTestData *data = (paTestData*) userData;
	char *out = (char*)outputBuffer;
	(void) inputBuffer;
	for (unsigned int i = 0; i < framesPerBuffer; ++i) {
		char b = beat(data->int_left_phase);
		*out++ = b;
		*out++ = b;
		++data->int_left_phase;
		++data->int_right_phase;
	}
	return paContinue;
}


